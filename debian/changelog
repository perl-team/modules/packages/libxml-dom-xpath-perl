libxml-dom-xpath-perl (0.14-4) unstable; urgency=medium

  [ Damyan Ivanov ]
  * declare conformance with Policy 4.1.3 (no changes needed)

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 28 Jun 2022 22:59:29 +0100

libxml-dom-xpath-perl (0.14-3) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Add patch from CPAN RT to fix issue caused by removal of the encoding
    pragma. (Closes: #826451)
  * Update years of packaging copyright.
  * Declare compliance with Debian Policy 4.0.0.

 -- gregor herrmann <gregoa@debian.org>  Sat, 24 Jun 2017 16:05:14 +0200

libxml-dom-xpath-perl (0.14-2) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: Changed: Switched Vcs-Browser field to ViewSVN
    (source stanza).
  * debian/control: Added: ${misc:Depends} to Depends: field.

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ gregor herrmann ]
  * Change my email address.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ Florian Schlichting ]
  * Switch to source format 3.0 (quilt)
  * Switch to short-form debian/rules
  * Refresh debian/copyright (using copyright-format 1.0)
  * Refresh d/control using dh-make-perl
    + build-Depend on debhelper 9
    + add myself to Uploaders
    + wrap-and-sort (build-)dependencies
  * Bump dh compat to level 9
  * Declare compliance with Debian Policy 3.9.6
  * Mark package autopkgtest-able

 -- Florian Schlichting <fsfs@debian.org>  Sat, 15 Aug 2015 11:15:00 +0200

libxml-dom-xpath-perl (0.14-1) unstable; urgency=low

  [ David Paleino ]
  * debian/control: removed myself from Uploaders

  [ gregor herrmann ]
  * debian/control: Added: Vcs-Svn field (source stanza); Vcs-Browser
    field (source stanza); Homepage field (source stanza). Removed:
    Homepage pseudo-field (Description); XS-Vcs-Svn fields.
  * New upstream release.
  * Remove utf8 patch (applied upstreams) and quilt framework.
  * Refresh debian/rules, no functional changes.
  * debian/control:
    - make (build) dependency on libxml-xpathengine-perl versioned
    - set Standards-Version to 3.7.3 (no changes)
    - wrap long line
  * debian/watch: extended regexp for matching upstream releases.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Wed, 16 Apr 2008 17:20:01 +0200

libxml-dom-xpath-perl (0.13-1) unstable; urgency=low

  [ David Paleino ]
  * Initial release (Closes: #442118)

  [ gregor herrmann ]
  * Add patch utf8 to make t/test_non_ascii.t work.

 -- gregor herrmann <gregor+debian@comodo.priv.at>  Wed, 19 Sep 2007 22:55:18 +0200
